pkgname=pylinuxwheel
_commit=5833f5fb80d9b17f87033a4d53e785ef16b579aa
pkgver=0.6.1
pkgrel=3
pkgdesc="A graphical program developed in gtk3 to configure your Logitech Steering Wheel in Linux"
url="https://odintdh.itch.io/pylinuxwheel"
arch=(x86_64)
license=(GPL3)
depends=(python-cairo python-gobject python-pyudev python-evdev gtk3)
makedepends=(gettext)
source=("git+https://gitlab.com/OdinTdh/pyLinuxWheel.git#commit=$_commit"
		"alt-fix-rules-file.patch"
	    "alt-added_ru_locale.patch"
	    "delete_unused_code.patch"
	    "lunaos-search_udev_rules_in_usr.patch")
sha256sums=('ed498bde898bfabfc3259611a182bfa343d78261fdf44ce121eb17d1cbf6a4a2'
            'ecd17586e11e69ab171d8a41b0caab3da22bd68ca4bca5760f48c7321510b939'
            'dd192051f064245a3c3dae0048246139572ca52c3395ee5bd80e03e40b7ec95f'
            'ad6a42ebe3343abe495c8ce0db35cb5f089034ea2f922cada6f7a4344a396e29'
            '92fc57f4e81e4fdf0368e999f1d0da7000aec77344a321117fba9191c28de306')

prepare() {
	cd pyLinuxWheel
	sed -i 's/Utility/Game/' data/desktop/io.itch.pyLinuxWheel.desktop
	sed -i 's/Exec=pyLinuxWheel/Exec=pylinuxwheel/' data/desktop/io.itch.pyLinuxWheel.desktop

	# fix udev rules (thx ALT Linux for patch)
	patch -d "$srcdir/pyLinuxWheel" -p1 -i "$srcdir/alt-fix-rules-file.patch"

    # added ru locale (thx ALT Linux for patch)
    patch -d "$srcdir/pyLinuxWheel" -p1 -i "$srcdir/alt-added_ru_locale.patch"
    
    # backpot upstream clean code commit
    patch -d "$srcdir/pyLinuxWheel" -p1 -i "$srcdir/delete_unused_code.patch"
    
    # search udev rules in /usr folder 
    patch -d "$srcdir/pyLinuxWheel" -p1 -i "$srcdir/lunaos-search_udev_rules_in_usr.patch"
}

build() {
    cd "$srcdir/pyLinuxWheel/locale/ru/LC_MESSAGES/"
    msgfmt -o pyLinuxWheel.mo pyLinuxWheel.po
}

package() {
	cd pyLinuxWheel
	
	mkdir -p "$pkgdir/usr/bin"
    mkdir "$pkgdir/usr/share"
    mkdir "$pkgdir/usr/share/applications"
	mkdir "$pkgdir/usr/share/pixmaps"
	mkdir -p "$pkgdir/usr/lib/udev/rules.d/"
    mkdir "$pkgdir/usr/share/pyLinuxWheel/"
    
	install -m 755 "pyLinuxWheel.py" "$pkgdir/usr/bin/pylinuxwheel"
	install -m 755 "data/desktop/io.itch.pyLinuxWheel.desktop" "$pkgdir/usr/share/applications/pyLinuxWheel.desktop"
	install -m 755 "data/img/icon-64-pyLinuxWheel.png" "$pkgdir/usr/share/pixmaps/pyLinuxWheel.png"

	cp -r "data" "$pkgdir/usr/share/pyLinuxWheel/"
	cp "data/rules/99-logitech-wheel-perms.rules" "$pkgdir/usr/lib/udev/rules.d/" 
	cp -r "locale" "$pkgdir/usr/share/"
}
